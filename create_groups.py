#!/usr/bin/env python3.8

import gitlab
import os
import yaml


if __name__ == "__main__":

    with open(r'config.yaml') as file:
        config = yaml.full_load(file)

    gl = gitlab.Gitlab('https://gitlab.com', private_token=os.getenv("GITLAB_TOKEN"))

    for user in config['users']:
        print(f"Create group for user {user}")
        group = gl.groups.create({
            'name': user,
            'path': user,
            'parent_id': config['semesterGroupId'],
            'visibility': 'public'
        })

        print(f"Invite user {user}@h-ka.de to group {user}")
        gl.http_post(f'/groups/{group.id}/invitations', {
            'email': user + "@h-ka.de",
            'access_level': 50
        })
